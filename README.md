# js-changeset

ecto like changesets for javascript/node.js

```javascript
const validate = (params = {}) =>
	new Changeset(params, ["name", "email", "password"])
		.validateRequired(["email", "password"])
		.validateFormat("email", /@/)
		.validateLength("password", { ">=": 6 });

var changeset = validate({ name: "Bob", email: "bob@mail.com" });
```
