var tape = require("tape"),
	Changeset = require("..");

function validateRequired(params) {
	return new Changeset(params, ["name"]).validateRequired(["name"]);
}

function validateFormat(params) {
	return new Changeset(params, ["email"]).validateFormat("email", /@/);
}

function validateLength(params) {
	return new Changeset(params, ["password"]).validateLength("password", {
		">=": 6
	});
}

tape("validateRequired", function(assert) {
	assert.equals(validateRequired({ name: "Bob" }).isValid(), true);

	var changeset = validateRequired({});
	assert.equals(changeset.isValid(), false);
	assert.deepEquals(changeset.getErrors(), {
		name: [{ message: "required", keys: [] }]
	});

	assert.end();
});

tape("validateFormat", function(assert) {
	assert.equals(validateFormat({ email: "bob@email.com" }).isValid(), true);

	var changeset = validateFormat({ email: "invalid" });
	assert.equals(changeset.isValid(), false);
	assert.deepEquals(changeset.getErrors(), {
		email: [{ message: "format", keys: [/@/] }]
	});

	assert.end();
});


tape("validateLength", function(assert) {
	assert.equals(validateLength({ password: "password" }).isValid(), true);

	var changeset = validateLength({ password: "pass" });
	assert.equals(changeset.isValid(), false);
	assert.deepEquals(changeset.getErrors(), {
		password: [{ message: "length", keys: ["gte", 6] }]
	});

	assert.end();
});
