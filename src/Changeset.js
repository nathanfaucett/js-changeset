var has = require("@nathanfaucett/has"),
	isNullOrUndefined = require("@nathanfaucett/is_null_or_undefined"),
	keys = require("@nathanfaucett/keys"),
	arrayForEach = require("@nathanfaucett/array-for_each");

var ChangesetPrototype = Changeset.prototype;

module.exports = Changeset;

function Changeset(fields, allowed) {
	var castFields = {},
		i = -1,
		il,
		field;

	fields = fields || {};
	allowed = allowed || [];
	il = allowed.length - 1;

	while (i++ < il) {
		field = allowed[i];

		if (has(fields, field)) {
			castFields[field] = fields[field];
		}
	}

	this._fields = castFields;
	this._allowed = allowed;
	this._changes = {};
	this._errors = {};
	this._valid = true;
}

ChangesetPrototype.isValid = function() {
	return this._valid;
};

ChangesetPrototype.hasChange = function(field) {
	return has(this._changes, field);
};

ChangesetPrototype.getField = function(field, defaultValue) {
	var changes = this._changes,
		fields = this._fields;

	if (has(changes, field)) {
		return changes[field];
	} else if (has(fields, field)) {
		return fields[field];
	} else {
		return defaultValue;
	}
};

ChangesetPrototype.getChange = function(field, defaultValue) {
	var changes = this._changes;

	if (has(changes, field)) {
		return changes[field];
	} else {
		return defaultValue;
	}
};

ChangesetPrototype.getError = function(field) {
	var errors = this._errors;

	if (has(errors, field)) {
		return errors[field];
	} else {
		return [];
	}
};

ChangesetPrototype.getAllowed = function() {
	return this._allowed;
};

ChangesetPrototype.getErrors = function() {
	var allowed = this._allowed,
		i = -1,
		il = allowed.length - 1,
		errors = {},
		field;

	while (i++ < il) {
		field = allowed[i];
		errors[field] = this.getError(field);
	}

	return errors;
};

ChangesetPrototype.getChanges = function() {
	var allowed = this._allowed,
		i = -1,
		il = allowed.length - 1,
		changes = {},
		field;

	while (i++ < il) {
		field = allowed[i];
		changes[field] = this.getField(field);
	}

	return changes;
};

ChangesetPrototype.addError = function(field, message, keys) {
	var errors = this._errors[field] || (this._errors[field] = []);

	errors.push({
		message: message,
		keys: keys || []
	});
	this._valid = false;

	return this;
};

ChangesetPrototype.putChange = function(field, value) {
	this._changes[field] = value;
	return this;
};

ChangesetPrototype.putChanges = function(params) {
	var allowed = this._allowed,
		i = -1,
		il = allowed.length - 1,
		changes = {},
		field;

	while (i++ < il) {
		field = allowed[i];

		if (has(params, field)) {
			changes[field] = params[field];
		}
	}

	this._changes = changes;

	return this;
};

ChangesetPrototype.clearChanges = function() {
	this._changes = {};
	return this;
};

ChangesetPrototype.clearErrors = function() {
	this._errors = {};
	this._valid = true;
	return this;
};

ChangesetPrototype.validateAcceptance = function(field) {
	var value = this.getField(field);

	if (value !== true) {
		this.addError(field, "acceptance");
	}

	return this;
};

function eq(a, b) {
	return a === b;
}
function neq(a, b) {
	return a !== b;
}
function gte(a, b) {
	return a >= b;
}
function lte(a, b) {
	return a <= b;
}
function gt(a, b) {
	return a > b;
}
function lt(a, b) {
	return a < b;
}

var COMPARISIONS = {
	"==": eq,
	">=": gte,
	"<=": lte,
	"!=": neq,
	">": gt,
	"<": lt,
	eq: eq,
	gte: gte,
	lte: lte,
	neq: neq,
	gt: gt,
	lt: lt
};
var COMPARISIONS_NAMES = {
	"==": "eq",
	">=": "gte",
	"<=": "lte",
	"!=": "neq",
	">": "gt",
	"<": "lt",
	eq: "eq",
	gte: "gte",
	lte: "lte",
	neq: "neq",
	gt: "gt",
	lt: "lt"
};
ChangesetPrototype.validateLength = function(field, opts) {
	var value = this.getField(field),
		optsKeys,
		i,
		il,
		op,
		length;

	value = isNullOrUndefined(value) ? [] : value;
	opts = opts || {};
	optsKeys = keys(opts);

	i = -1;
	il = optsKeys.length - 1;
	while (i++ < il) {
		op = optsKeys[i];

		if (has(COMPARISIONS, op)) {
			length = +opts[op];

			if (!COMPARISIONS[op](+value.length, length)) {
				this.addError(field, "length", [
					COMPARISIONS_NAMES[op],
					length
				]);
			}
		} else {
			throw new TypeError("No comparision for " + COMPARISIONS_NAMES[op]);
		}
	}

	return this;
};

ChangesetPrototype.validateRequired = function(fields) {
	var _this = this;

	arrayForEach(fields, function each(field) {
		var value = _this.getField(field);

		if (isNullOrUndefined(value)) {
			_this.addError(field, "required");
		}
	});

	return this;
};

ChangesetPrototype.validateFormat = function(field, regex) {
	var value = this.getField(field);

	value = isNullOrUndefined(value) ? "" : value.toString();

	if (!regex.test(value)) {
		this.addError(field, "format", [regex]);
	}

	return this;
};
